const mainColors = {
    green1: '#0BCAD4',
    dark1: '#112340',
    dark2: '#495A75',
    grey1: '#7D8797',
    grey2: '#E9E9E9',
    grey3: '#EDEEF0',
    grey4: '#B1B7C2',
    grey5: '#9096A3',
    grey6: '#D9DCE2',

    
    blue1 : '#0066CB',
    black1: '#000000' ,
    black2: 'rgba(0,0,0,0.5)',
    red1: '#E06379'
}

export const colors = {
    primary: mainColors.green1,
    secondary: mainColors.dark1,
    tertiary: mainColors.blue1,
    green1: mainColors.green1,
    white: 'white',
    black: 'black',
    greyBorder : '#B1B7C2',
    

    text: {
        primary: mainColors.dark1,
        secondary: mainColors.grey1,
        menuInactive: mainColors.dark2,
        menuActive : mainColors.green1,
    },
    button: {
        primary: {
            background: mainColors.green1,
            text: 'white',
        },
        secondary: {
            background: 'white',
            text: mainColors.dark1,
        },
        disable: {
            background: mainColors.grey3,
            text: mainColors.grey4,
            
        }
    },
    border1: {
        pri:mainColors.grey4,
        sec: mainColors.grey5,
        third : mainColors.grey6,
        
    },
    border: mainColors.grey2,
    borderSearch: mainColors.grey4,
    loadingBackground: mainColors.black2,
    error: mainColors.red1,
}