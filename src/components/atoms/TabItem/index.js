import React from 'react';
import { StyleSheet, Text, TouchableOpacity } from 'react-native';
import { IconHome, IconHomeActive, IconMaps, IconMapsActive, IconProfile, IconProfileActive } from '../../../assets';
import { colors, fonts } from '../../../utils';

const TabItem = ({ tittle, active, onPress, onLongPress }) => {
    const Icon = () => {
        if (tittle === 'Home') {
            return active ? <IconHomeActive /> : <IconHome />
        }
        if (tittle === 'Maps') {
            return active ? <IconMapsActive /> : <IconMaps />
        }
        if (tittle === 'Profile') {
            return active ? <IconProfileActive /> : <IconProfile />
        }
        return <IconHome />
    }
    return (
        <TouchableOpacity
            style={styles.container}
            onPress={onPress}
            onLongPress={onLongPress}
        >
            <Icon />
            <Text style={styles.text(active)}>{tittle}</Text>
        </TouchableOpacity>
    )
}
export default TabItem;

const styles = StyleSheet.create({
    container: {
        alignItems: 'center',
        
    },
    text: (active) => ({
        fontSize: 10,
        color: active ? colors.text.menuActive : colors.text.menuInactive, 
        fontFamily: fonts.primary[600],
        marginTop: 4,
        

    })
})
