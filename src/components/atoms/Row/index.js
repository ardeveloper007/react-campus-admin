import React from 'react'
import { StyleSheet, Text, View } from 'react-native'
import { Gap } from '../../../components'
import { colors, fonts } from '../../../utils'

const Row = ({ tb1, tb2 }) => {
    return (
        <>
            <View style={styles.table}>
                <View style={styles.tb1}>
                    <Text style={styles.title}>{tb1}</Text>
                </View>

                <View>
                    <Text>{tb2}</Text>
                </View>
            </View>

        </>
    )
}
export default Row;

const styles = StyleSheet.create({
    tb1: {
        width: '50%',
    },
    title: {
        fontFamily: fonts.primary[900],
        color: colors.text.primary,
        fontSize: 14,
    },
    table: {
        fontSize: 14,
        fontFamily: fonts.primary[500],
        flexDirection: 'row',
        color: colors.text.primary,
        marginBottom: 8
    }
})
