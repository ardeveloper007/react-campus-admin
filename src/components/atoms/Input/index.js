import { Picker } from '@react-native-picker/picker';
import React from 'react'
import { useState } from 'react';
import { StyleSheet, Text, TextInput, View } from 'react-native'
import { colors, fonts } from '../../../utils';


const Input = ({ 
  label,
  value,
  onChangeText,
  secureTextEntry,
  disable,
  select,
  onValueChange,
  selectItem,
  textArea,
  row,
  
  ...restProps
  
}) => {
  const [border, setBorder] = useState(colors.border)
  const onFocusForm = () => {
    setBorder(colors.tertiary)
  }
  const onBlurForm = () => {
    setBorder(colors.border)
  }

  const onFocusRow = () => {
    setBorder(colors.tertiary)
  }
  const onBlurRow = () => {
    setBorder(colors.border1.sec)
  }
  if (row) {
    <View style={styles.conRow(border)}>
      <TextInput style={styles.row}
        placeholder={label}
        placeholderTextColor="#9FA5AF"
        onFocus={onFocusRow}
        onBlurRow={onBlurRow}
      />
    </View>
  }

  if (textArea) {
    return (
      <View>
        <Text style={styles.label}>{label}</Text>
        <TextInput
          style={styles.input(border)}
          onFocus={onFocusForm}
          onBlur={onBlurForm}
          value={value}
          onChangeText={onChangeText}
          secureTextEntry={secureTextEntry}
          multiline={true}
          numberOfLines={4}
        />
      </View>
    )
  }
  if (select) {
    return (
      <View>
        <Text style={styles.label}>{label}</Text>
        <View style={styles.picker}>
          <Picker selectedValue={value} onValueChange={onValueChange} dropdownIconColor= 'grey'>
            {selectItem.map((item) => {
              return (
                <Picker.Item
                  style={styles.styPicker}
                  key={item.id}
                  label={item.label}
                  value={item.value}
                />
              )
            })}
          </Picker>
        </View>
      </View>
    )
  }
  return (
    <View>
      <Text style={styles.label}>{label}</Text>
      <TextInput
        style={styles.input(border)}
        onFocus={onFocusForm}
        onBlur={onBlurForm}
        value={value}
        onChangeText={onChangeText}
        secureTextEntry={secureTextEntry}
        // editable={false} selectTextOnFocus={false}
        {...restProps}
      />
    </View>
  )
}
export default Input;

const styles = StyleSheet.create({
  styPicker:{
    backgroundColor: colors.white,
    color: colors.text.primary,
    fontSize: 14,
    fontFamily: fonts.primary[600],
  },
  input: border => ({
    borderWidth: 1,
    borderColor: border,
    borderRadius: 10,
    padding: 12,
    color: colors.text.primary
  }),
  label: {
    fontSize: 16,
    color: colors.text.secondary,
    marginBottom: 6,
    fontFamily: fonts.primary[400]
  },
  picker: {
    borderWidth: 1,
    borderColor: colors.border,
    borderRadius: 10,
    paddingHorizontal: 4,
  },
  row: {
    fontFamily: fonts.primary[600],
    color: colors.text.primary,
    fontSize: 14,
    flex: 1,
  },
  conRow: border => ({
    flexDirection: 'row',
    borderWidth: 1,
    borderColor: colors.border1.sec,
    borderRadius: 10,
    marginVertical: 8,
    marginHorizontal: 24,
    paddingHorizontal: 8,
    alignItems: 'center',
    height: 40,
    borderColor: border,
    paddingHorizontal: 8,
  }),
})
