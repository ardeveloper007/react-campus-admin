import React from 'react'
import { StyleSheet, Text, View, Image, TouchableOpacity } from 'react-native'
import { ILUpr } from '../../../assets';
import { fonts, colors } from '../../../utils';
const GaleriFasilitasUmum = ({gambar, ruangan, onPress}) => {
    return (
        <TouchableOpacity style={styles.container} onPress={onPress}>
            <Image source={{ uri: gambar }} style={styles.image} />
            <Text style={styles.title}>{ruangan}</Text>
        </TouchableOpacity>
    )
}
export default GaleriFasilitasUmum;

const styles = StyleSheet.create({
    container: {
        marginBottom: 7,
        marginRight: 8
    },
    title: {
        fontFamily: fonts.primary[500],
        fontSize: 14,
        color: colors.text.secondary
    },
    image: {
        width: 300,
        height: 150,
        borderRadius: 10,
        borderWidth: 2,
        borderColor: colors.border1.third,
    },
})
