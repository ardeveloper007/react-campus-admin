import React, { useState } from 'react';
import {
  Alert, StyleSheet, View
} from 'react-native';
import { Gap, List, ProfileDetail } from '../../components';
import { Fire } from '../../config';
import { colors, showError } from '../../utils';

const Profile = ({ navigation }) => {
  const [userId, setUserId] = useState([])

  const [profile, setProfile] = useState({
    name: "",
    email: ''
  })

  const signOut = () => {
    Alert.alert(
      "Logging out",
      `Are you sure`,
      [
        { text: 'NO', style: 'cancel' },
        {
          text: 'YES', onPress: () => {
            Fire.auth()
              .signOut()
              .then(res => {
                console.log('success sign out')
                navigation.replace('Login')
              }).catch(err => {
                showError(err.message)
              })
          }
        },
      ]
    );

  }

  return (
    <View style={styles.container}>
      {profile.name.length > 0 && (

        <ProfileDetail nama={profile.name} email={profile.email} />
      )}
      <Gap height={10} />
      <List
        name="Sign Out"
        desc="You out from this account"
        type="next"
        icon="logout"
        onPress={() => signOut()}
      />
      <List
        name="Developer"
        desc="Detail about developer"
        type="next"
        icon="dev"
        onPress={() => navigation.navigate('Developer')}
      />
    </View>
  )
}
export default Profile;

const styles = StyleSheet.create({
  container: {
    paddingHorizontal: 40,
    backgroundColor: colors.white,
    flex: 1
  }
})
