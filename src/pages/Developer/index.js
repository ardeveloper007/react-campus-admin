import React from 'react'
import { StyleSheet, Text, View, Image } from 'react-native'
import { Header, ProfileDetail, Gap } from '../../components'
import { colors, fonts } from '../../utils'
import { me } from '../../assets'

const Developer = ({navigation})  => {
    return (
        <View style={styles.page} >
            <Header tittle="Developer" onPress={() => navigation.goBack()} />
            <View style={styles.container}>
                <View style={styles.photo}>
                    <View style={styles.avatarWrapper}>
                        <Image style={styles.avatar} source={me} />
                    </View>
                    <Gap height={16} />
                    <Text style={styles.title}>Diana</Text>
                    <Text style={styles.email}>DBC 118 076</Text>
                </View>
            </View>
        </View>
    )
}
export default Developer;

const styles = StyleSheet.create({
    page: {
        flex: 1
    },
    title: {
        fontSize: 24,
        color: colors.text.primary,
        fontFamily: fonts.primary[600],
        textAlign: 'center'
    },
    email: {
        fontSize: 18,
        fontFamily: fonts.primary.normal,
        textAlign: 'center',
        color: colors.text.secondary,
        marginTop: 4,
    },
    container: {
        paddingHorizontal: 40,
        backgroundColor: colors.white,
        flex: 1,
        justifyContent: 'center',
        alignContent: 'center',
        alignItems: 'center',
    },
    photo: {
        marginVertical: 20,
        alignItems: 'center',
        justifyContent: 'center'
    },
    avatar: {
        width: 110,
        height: 110,
        borderRadius: 110 / 2
    },
    avatarWrapper: {
        width: 130,
        height: 130,
        borderWidth: 1,
        borderColor: colors.border,
        borderRadius: 130 / 2,
        alignItems: 'center',
        justifyContent: 'center'
    },
})
