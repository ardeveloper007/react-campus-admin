import React from 'react'
import { StyleSheet, Text, View, ScrollView, Image } from 'react-native'
import {
  Button,
  Header,
  ListDetailRoom,
  Search,
  Gap,
  Row
} from '../../components'
import { IconImage, IconMaps } from '../../assets'
import { colors, fonts } from '../../utils'

const DetailRoom = ({ navigation, route }) => {
  const dataRoom = route.params
  console.log('isi data', dataRoom)

  return (
    <>
      <View style={styles.header}>
        <View>
          <Header tittle={dataRoom.item.data.Ruangan} onPress={() => navigation.goBack()} />
        </View>
        <View>
          <IconMaps />
          <Gap width={15} />
        </View>
      </View>
      <View style={styles.content}>
        <Image source={{ uri: dataRoom.item.data.Gambar }} style={styles.img} />
        <ScrollView>
          <Row tb1={'KATEGORI'} tb2={dataRoom.item.data.Kategori} />
          <Row tb1={'PRODI'} tb2={dataRoom.item.data.Prodi} />
          <View style={styles.des}>
            <Text style={styles.title}>DESKRIPSI</Text>
            <Text>{dataRoom.item.data.Deskripsi}</Text>
          </View>
          <Gap height={170} />
        </ScrollView>
      </View>
    </>
  )
}
export default DetailRoom
const styles = StyleSheet.create({
  header: {
    flexDirection: 'row',

  },
  img: {
    width: 100,
    height: 150,
    position: 'absolute',
    bottom: 0,
    left: 40,
    zIndex: 30,
    borderRadius: 5,
  },
  title: {
    fontFamily: fonts.primary[900],
    color: colors.text.primary,
    fontSize: 14,
  },
  des: {
    fontSize: 14,
    fontFamily: fonts.primary[500],
    color: colors.text.primary,
    marginBottom: 8,
    textAlign: 'justify',
  },
  content: {
    paddingHorizontal: 40,
    paddingTop: 0,
    flex: 1,
    backgroundColor: colors.white
  }
})
