import React from 'react'
import { useState } from 'react'
import {
    ScrollView,
    StyleSheet,
    View
} from 'react-native'
import {
    Button,
    Gap,
    Header,
    Input
} from '../../components'
import { colors, showError, storeData, useForm } from '../../utils'
import { Fire } from '../../config'
import { useDispatch } from 'react-redux'

const AddAdmin = ({ navigation }) => {
    const dispatch = useDispatch()
    const [form, setForm] = useForm({
        nama: '',
        email: '',
        password: '',
    })
    const onContinue = () => {
        console.log('hasil : ', form)
        dispatch({ type: 'SET_LOADING', value: true })
        Fire.auth()
            .createUserWithEmailAndPassword(form.email, form.password)
            .then((succes) => {
                setForm('reset')
                const data = {
                    nama: form.nama,
                    email: form.email,
                    password: form.password,
                    uid: succes.user.uid
                }
                Fire.database()
                    .ref(`admin/${succes.user.uid}/`)
                    .set(data)
                storeData('admin', data)
                navigation.navigate('ListAdmin')
                dispatch({ type: 'SET_LOADING', value: false })
                console.log('register success :', succes)
            })
            .catch((err) => {
                showError(err.message)
                dispatch({ type: 'SET_LOADING', value: false })
            })

    }

    return (
        <View style={styles.page}>
            <Header onPress={() => navigation.goBack()} tittle={"Register"} />
            <View style={styles.content}>
            <Input
                    label="Nama"
                    value={form.nama}
                    onChangeText={(value) => setForm('nama', value)}
                />
                <Gap height={24} />
                <Input
                    label="Email"
                    value={form.email}
                    onChangeText={(value) => setForm('email', value)}
                />
                <Gap height={24} />
                <Input
                    label="Password"
                    value={form.password}
                    onChangeText={(value) => setForm('password', value)}
                    secureTextEntry
                />
                <Gap height={40} />
          <Button tittle="Continue" onPress={onContinue} />
            </View>
        </View>
    )
}
export default AddAdmin;

const styles = StyleSheet.create({
    page: {
        backgroundColor: colors.white,
        flex: 1,
    },
    content: {
        padding: 40,
        paddingTop: 0
    }
})
