import React from 'react'
import { StyleSheet, Text, View, Image, ScrollView } from 'react-native'
import { ILUpr } from '../../assets'
import { Button, Gap, Input, Link } from '../../components'
import { colors, fonts, showSuccess, showError, useForm, storeData } from '../../utils'
import { Fire } from '../../config'
import { useDispatch } from 'react-redux'

const Login = () => {
  const dispatch = useDispatch()
  const [form, setForm] = useForm({
    email: '',
    password: ''
  })

  const onLogin = () => {
    console.log('isi login ', form)
    dispatch({ type: 'SET_LOADING', value: true })
    Fire.auth()
      .signInWithEmailAndPassword(form.email, form.password)
      .then(res => {
        console.log('succes : ', res)
        dispatch({ type: 'SET_LOADING', value: false })
        Fire.database()
          .ref(`admin/${res.user.uid}/`)
          .once('value')
          .then(resDB => {
            console.log('data user: ', resDB.val())
            if (resDB.val()) {
              storeData('admin: ', resDB.val())
              navigation.replace('MainAppUser')
            }
          })

      })
      .catch(err => {
        console.log('error : ', err)
        dispatch({ type: 'SET_LOADING', value: false })
        showError(err.message)
      })
  }
  return (
    <View style={styles.container}>
      <ScrollView>
        <View style={styles.page}>
          <Image style={{ height: 100, width: 100 }} source={ILUpr} />
          <Text style={styles.tittle}>Login admin</Text>
          <Input label="Email Address"
            value={form.email}
            onChangeText={value => setForm('email', value)}
          />
          <Gap height={24} />
          <Input label="Password"
            value={form.password}
            onChangeText={value => setForm('password', value)}
            secureTextEntry
          />
          <Gap height={20} />
          <Button tittle="Sign In" onPress={onLogin} />
          <Gap height={30} />
          
        </View>
      </ScrollView>
    </View>
  )
}
export default Login;

const styles = StyleSheet.create({
  container: {
    backgroundColor: colors.white,
    flex: 1
  },
  page: {
    padding: 40,
    backgroundColor: colors.white,
    flex: 1
  },
  tittle: {
    fontSize: 20,
    fontFamily: fonts.primary[500],
    color: colors.text.primary,
    marginTop: 40,
    marginBottom: 40,
    maxWidth: 170,
  }
})
