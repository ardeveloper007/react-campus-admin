import Login from './Login'
import Home from './Home'
import Maps from './Maps'
import Profile from './Profile'
import AddMaps from './AddMaps'
import AddRoom from './AddRoom'
import AdminDetailRoom from './AdminDetailRoom'
import DetailRoom from './DetailRoom'
import ListRoom from './ListRoom'
import SearchMaps from './SearchMaps'
import DetailPhoto from './DetailPhoto'
import DetailMaps from './DetailMaps'
import ListUser from './ListUser'
import ListAdmin from './ListAdmin'
import AddAdmin from './AddAdmin'
import DetailUser from './DetailUser'
import Splash from './Splash'
import Developer from './Developer'
import EditProfile from './EditProfile'

export{
    EditProfile,
    Developer,
    Splash,
    Login,
    Home,
    Maps,
    Profile,
    AddMaps,
    AddRoom,
    AdminDetailRoom,
    DetailRoom,
    ListRoom,
    SearchMaps,
    DetailPhoto,
    DetailMaps,
    ListUser,
    ListAdmin,
    AddAdmin,
    DetailUser

}