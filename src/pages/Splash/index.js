import React, { useEffect } from 'react'
import {
    Image, StyleSheet, Text,
    View
} from 'react-native'
import { ILUpr } from '../../assets'
import { Gap } from '../../components'
import { Fire } from '../../config'
import {
    colors,
    fonts
} from '../../utils'

const Splash = ({ navigation }) => {
    useEffect(() => {
        setTimeout(() => {
            Fire.auth()
                .onAuthStateChanged(user => {
                    if (user) {
                        console.log('pengguna : ', user)
                        console.log('isi uid anda: ',user.uid)
                        navigation.replace('MainApp')
                    } else {
                        navigation.replace('Login')
                    }
                })
        }, 1000)
    }, [navigation])
    return (
        <View style={styles.page}>
            <Image style={{ height: 100, width: 100 }} source={ILUpr} />
            <Gap height={12} />
            <Text style={styles.tittle}>University of Palangka Raya</Text>
        </View>
    )
}
export default Splash;

const styles = StyleSheet.create({
    page: {
        backgroundColor: colors.white,
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
    tittle: {
        fontFamily: fonts.primary[500],
        fontSize: 20,
        color: colors.text.primary,
        marginTop: 20,
    }

})
