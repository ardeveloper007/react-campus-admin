import React, { useEffect, useState } from 'react'
import { Image, PermissionsAndroid, StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import Geolocation from 'react-native-geolocation-service'
import MapView, { Marker } from 'react-native-maps'
import { useDispatch } from 'react-redux'
import { IconMapsUser, IconRefresh, ILNullPhoto } from '../../assets'
import { Button, Gap, Input } from '../../components'
import { Fire } from '../../config'
import { colors, showError, showSuccess } from '../../utils'

const AddMaps = ({ navigation, route }) => {
  const dispatch = useDispatch()
  const { Gambar, Kategori, Prodi, Ruangan, Deskripsi } = route.params
  const { photo } = useState(ILNullPhoto)
  const [latitude, setLatitude] = useState(0)
  const [longitude, setLongitude] = useState(0)
  const [latitudeDelta] = useState(0.0001)
  const [longitudeDelta] = useState(0.0021)
  const [accuracy, setAccuracy] = useState(0)
  const init = {
    latitude,
    longitude,
    latitudeDelta,
    longitudeDelta,
  }

  console.log('isi init: ', init)

  useEffect(() => {
    permission();
  }, [])

  const permission = async () => {
    try {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
        {
          title: "Allow 'Location' to access your location",
          message:
            "This app need access to your location! ",
          buttonNeutral: "Ask Me Later",
          buttonNegative: "Don't Allow",
          buttonPositive: "Allow"
        }
      );
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        console.log("Access your location");

        Geolocation.getCurrentPosition(
          (position) => {
            console.log(position);
            { setLongitude(position.coords.longitude) }
            { setLatitude(position.coords.latitude) }
            { setAccuracy(position.coords.accuracy) }

          },
          (error) => {
            console.log(error.code, error.message);
          },

          {
            enableHighAccuracy: true,
            timeout: 3000,
            maximumAge: 10000,
            interval: 1000,
            accuracy: {
              android: 'high'
            },
            forceRequestLocation: true
          }
        );
        if (accuracy > 20) {
          console.log('accuracy high :')
          showSuccess('Accuracy your location is high')
        } else {
          showError('Accuracy your location is low, Try again')
        }

      } else {
        console.log("Access location permission denied");
      }

    }
    catch (err) {
      console.warn(err);
    }

  };
  console.log('accuracy : ', accuracy)
  const onContinue = () => {
    dispatch({ type: 'SET_LOADING', value: true })
    navigation.replace('ListRoom')
    if (Prodi === '') {
      Fire.database()
        .ref(`rooms`)
        .push(
          {
            Gambar: Gambar,
            Kategori: Kategori,
            Ruangan: Ruangan,
            Deskripsi: Deskripsi,
            Coordinate: {
              latitude: latitude,
              longitude: longitude
            }
          }
        ).then(() => {
          console.log('succes dimasukkan ke database')
          dispatch({ type: 'SET_LOADING', value: false })
        }).catch((err) => {
          showError(err.message)
          dispatch({ type: 'SET_LOADING', value: false })
        })
    } else {
      Fire.database()
        .ref(`rooms`)
        .push(
          {
            Gambar: Gambar,
            Kategori: Kategori,
            Prodi: Prodi,
            Ruangan: Ruangan,
            Deskripsi: Deskripsi,
            Coordinate: {
              latitude: latitude,
              longitude: longitude
            }

          }
        ).then(() => {
          console.log('succes dimasukkan ke database')
          dispatch({ type: 'SET_LOADING', value: false })
        }).catch((err) => {
          showError(err.message)
          dispatch({ type: 'SET_LOADING', value: false })
        })
    }
  }


  return (
    <View style={styles.container}>
      <View style={styles.geo}>
        <View style={styles.node}>
          <View style={styles.long}>
            <TouchableOpacity style={styles.refresh} onPress={permission}>
              <IconRefresh />
            </TouchableOpacity>
            <View>
              <Text>Latitude : {latitude} , Longitude : {longitude}</Text>
            </View>
          </View>
        </View>

      </View>
      <MapView
        style={{ flex: 1 }}
        initialRegion={{
          latitude: -2.2158532,
          longitude: 113.8985776,
          latitudeDelta: 0.01,
          longitudeDelta: 0.01,
        }}
      >
        <Marker
          coordinate={{ latitude: latitude, longitude: longitude }}
          title="My Location"
        >
          <Image source={IconMapsUser} style={{ height: 30, width: 30 }} />
        </Marker>
      </MapView>
      <View style={styles.btn}>
        <View style={styles.maps}>
          <Button icon="ceklis-active" type="icon-only" onPress={onContinue} />
        </View>
      </View>
    </View>
  )
}
export default AddMaps;

const styles = StyleSheet.create({
  btn: {
    position: 'absolute',
    bottom: 5,
    right: 5,
  },
  long: {
    flexDirection: 'row'
  },
  refresh: {
    width: 30,
    height: 25,
    justifyContent: 'center',
    alignContent: 'center',
    alignItems: 'center'
  },
  container: {
    flex: 1,
    backgroundColor: 'white'
  },
  node: {
    flex: 1,
    paddingVertical: 8,
    margin: 12,
  },
  geo: {
    flexDirection: 'row',
    alignItems: 'center',
    alignContent: 'center',
    justifyContent: 'center',
  },
  maps: {
    alignItems: 'center',
    alignContent: 'center',
    justifyContent: 'center',
    backgroundColor: colors.secondary,
    padding: 12,
    width: 60,
    height: 60,
    borderRadius: 57 / 2,
    borderWidth: 3,
    borderColor: colors.primary,

  },
})
